#### This code is for:
- Blue Pill (STM32F103C8T6)
- TTP223 touch sensor 
- MLX90614
- SSD1306 128x64 OLED display

There are **CubeMX** (ver. 5.6.1) and **IAR EWARM** (ver. 8.22.1) projects inside

Library for SSD1306 - *Src/oled.c*
